import React, { useState } from 'react';
import './log.css';

import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';

import Table from 'react-bootstrap/Table';

import Modal from 'react-bootstrap/Modal';

import Form from 'react-bootstrap/Form';

function Log() {

  const [show, setShow] = useState(false);

  const [modalData, setModalData] = useState({ title:"Legg til kjøp", buttonText:"Legg til kjøp", showSaleInput:"none"});

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function handleBuy() {
    setModalData({ title:"Legg til kjøp", buttonText:"Legg til kjøp", showSaleInput:"none" });
    setShow(true);
  }

  function handleSell() {
    setModalData({ title:"Legg til salg", buttonText:"Legg til salg", showSaleInput:"" });
    setShow(true);
  }

    return (
      <>
        <div className="container">
          <br/>
          <p>Logghistorikk</p>
          <div className="d-flex justify-content-center">
            <button type="button" class="btn btn-success m-2" onClick={handleBuy}>Legg til kjøp</button>
            <button type="button" class="btn btn-danger m-2" onClick={handleSell}>Legg til salg</button>
          </div>

          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Ticker</th>
                <th>Askje</th>
                <th>Beholdning</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>AK1</td>
                <td>Aksje 1</td>
                <td>5</td>
              </tr>
              <tr>
                <td>AK2</td>
                <td>Askje 2</td>
                <td>10</td>
              </tr>
              <tr>
                <td>AK3</td>
                <td>Askje 3</td>
                <td>200</td>
              </tr>
            </tbody>
          </Table>

        </div>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header>
            <Modal.Title>{modalData.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3">
                <Form.Label>Ticker</Form.Label>
                <Form.Control className="mb-3" type="text" placeholder="EQNR" />
                <Form.Label>Antall aksjer</Form.Label>
                <Form.Control className="mb-3" type="number" />
                <Form.Label>Kjøpsdato</Form.Label>
                <Form.Control className="mb-3" type="date" />
                <Form.Label>Kjøpspris</Form.Label>
                <Form.Control className="mb-3" type="number" />
              </Form.Group>
              <Form.Group className="mb-3" style={ {display: modalData.showSaleInput}}>
                <Form.Label>Salgsdato</Form.Label>
                <Form.Control className="mb-3" type="date" />
                <Form.Label>Salgspris</Form.Label>
                <Form.Control type="number" />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={handleClose}>
              {modalData.buttonText}
            </Button>
            <Button variant="secondary" onClick={handleClose}>
              Avbryt
            </Button>
          </Modal.Footer>
        </Modal>

      </>

      
    )
  }
export default Log
