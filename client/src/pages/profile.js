import React, { useContext } from 'react';
import './profile.css';

import { getUserProfile } from '../common/restclient';

import { UserContext } from '../contexts/UserContext';

function Profile() {

  const user = useContext(UserContext);

  return (
    <div className="profileCard">
      <h1>Profil</h1>
      <br />
      <p>{user.firstName}</p>
      <p>{user.lastName}</p>
      <p>{user.userName}</p>
      <p>{user.email}</p>
    </div>
  )
}

export default Profile
