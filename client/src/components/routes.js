import React from "react";
import { Route, Switch } from "react-router-dom";

import Home  from '../pages/home'
import Log from '../pages/log'
import Profile from '../pages/profile'



export default function Routes() {
  return (
    <Switch>
        <Route exact path="/"><Home /></Route>
        <Route path="/home"> <Home /></Route>
        <Route path="/log"> <Log /></Route> 
        <Route path="/profile"> <Profile /></Route> 
    </Switch>
  );
}

