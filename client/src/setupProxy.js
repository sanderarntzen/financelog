const { createProxyMiddleware } = require('http-proxy-middleware');



// Dette fixer noe som kan synes som en bug i http-proxy-middleware
// uten denne vil proxyen sette X-Forwarded-Proto til 'http,https'
// og det vil medføre at openLiberty konstruerer en ugyldig url når det 
// skal videresendes etter vellykt pålogging

const onProxyReq = function (proxyReq, req, res) {
  proxyReq.setHeader('X-Forwarded-Proto', 'https');
};


module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://localhost:9080',
      changeOrigin: false,
      xfwd:true
      //Kommenter inn linja under hvis man kjører på gitpod
      // ,onProxyReq: onProxyReq
    })
  );
};