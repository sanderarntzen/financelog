import React, { createContext, useEffect, useState } from "react";
import "./index.css";
import { Nav, Navbar } from "react-bootstrap";
import { ReactComponent as Logo } from "./images/logo.svg";
import Routes from "./components/routes";

import "bootstrap/dist/css/bootstrap.min.css";
//import { getUserProfile } from "./common/restclient";

import { UserContext } from "./contexts/UserContext";

export default function App() {

  const [user, setUser] = useState({firstName:"laster...", lastName:"", userName:"", email:""});

  useEffect(async () => {
    await fetch('api/user')
      .then(res => res.json())
      .then(data => setUser(data));
    //console.log("result ", result.data);
    //setUser(result.data);
    
  }, []);


  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand href="#">
          <Logo
            alt=""
            width="30"
            height="30"
            className="d-inline-block align-top"
          />
          Financelog
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/#home">Hjem</Nav.Link>
            <Nav.Link href="/#log">Logg</Nav.Link>
          </Nav>
          <Nav>
            <Nav.Link href="/#profile">Profil</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <UserContext.Provider value={user}>
        <Routes />
      </UserContext.Provider>
    </div>
  );
}
