package no.bwa.financelog.utils;

// import com.fasterxml.jackson.annotation.JsonInclude.Include;
// import com.fasterxml.jackson.databind.DeserializationFeature;
// import com.fasterxml.jackson.databind.ObjectMapper;
// import com.fasterxml.jackson.databind.SerializationFeature;
// import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
// import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomObjectMapper {
    
    private static final Logger logger = LoggerFactory.getLogger(CustomObjectMapper.class);    

    // public static ObjectMapper mapper = new ObjectMapper();
    
    // static {
    //     logger.info("CustomObjectMapper.x using jdk8 datatypes...");
        
    //     mapper.registerModule(new JavaTimeModule());
    //     mapper.registerModule(new Jdk8Module());

        
    //     mapper.setSerializationInclusion(Include.NON_NULL);
    //     mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    //     mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    //     mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    //     //mapper.enable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);
    // }
}