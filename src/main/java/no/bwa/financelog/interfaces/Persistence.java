package no.bwa.financelog.interfaces;

import java.util.List;

import no.bwa.financelog.models.StockTransaction;
import no.bwa.financelog.models.UserProfile;

public interface Persistence {
    void addTransaction(StockTransaction stockTransaction);
    List<StockTransaction> getTransactions(String username);
    UserProfile getUserProfile(String username);
    void addUserProfile(UserProfile userProfile);
}
