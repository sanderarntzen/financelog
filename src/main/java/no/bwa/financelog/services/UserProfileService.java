package no.bwa.financelog.services;

import javax.inject.Inject;

import com.ibm.websphere.security.jwt.Claims;
import com.ibm.websphere.security.social.UserProfileManager;

import no.bwa.financelog.interfaces.Persistence;
import no.bwa.financelog.models.UserProfile;

public class UserProfileService {

    @Inject
    Persistence persistenceService;

    public UserProfile getUserProfile(String username) {
        UserProfile userProfile = persistenceService.getUserProfile(username);

        if(userProfile != null) {
            return userProfile;
        } else {
            com.ibm.websphere.security.social.UserProfile _userProfile = UserProfileManager.getUserProfile();
            Claims claims = _userProfile.getClaims();

            no.bwa.financelog.models.UserProfile user = new no.bwa.financelog.models.UserProfile(
                username,
                claims.get("email").toString(), 
                claims.get("given_name").toString(), 
                claims.get("family_name").toString()
                );
            persistenceService.addUserProfile(user);
            return user;
            
        }
    }
    
}
