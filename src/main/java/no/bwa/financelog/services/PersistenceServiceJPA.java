package no.bwa.financelog.services;


import javax.enterprise.inject.Default;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.bwa.financelog.entities.FinanceTransactionEntity;

@Default // betyr at CDI bruker denne som default impl av PersistenceService
@Singleton

public class PersistenceServiceJPA {

    private static final Logger log = LoggerFactory.getLogger(PersistenceServiceJPA.class);
    
    @PersistenceContext(name = "jpa-persistenceunit")
    private EntityManager em;

    public String addTransaction(FinanceTransactionEntity transaction) {
        em.persist(transaction);
        log.info("Lagret transaksjon {} OK", transaction.getId());
        return transaction.getId();
    }
}
