package no.bwa.financelog.services;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import lombok.extern.slf4j.Slf4j;
import no.bwa.financelog.interfaces.Persistence;
import no.bwa.financelog.models.StockTransaction;
import no.bwa.financelog.models.UserProfile;

import static com.mongodb.client.model.Filters.eq;

@Slf4j
public class PersistenceServiceMongo implements Persistence {

    @Inject
    MongoDatabase db;

    private MongoCollection<StockTransaction> collectionOfTransactions;
    private MongoCollection<UserProfile> collectionOfUserProfiles;

    
    @PostConstruct
    public void init() {
        log.info("PersistenceServiceMongo init");
        this.collectionOfTransactions = db.getCollection("transactions", StockTransaction.class);
        this.collectionOfUserProfiles = db.getCollection("userprofiles", UserProfile.class);
    }

    @Override
    public void addTransaction(StockTransaction stockTransaction) {
        //MongoCollection<StockTransaction> collectionOfTransactions = db.getCollection("transactions", StockTransaction.class);

        stockTransaction.setId(UUID.randomUUID().toString());

        collectionOfTransactions.insertOne(stockTransaction);
        log.info("Transaction lagret {}", stockTransaction );
    }

    @Override
    public List<StockTransaction> getTransactions(String username) {
        //MongoCollection<StockTransaction> collectionOfTransactions = db.getCollection("transactions", StockTransaction.class);

        FindIterable<StockTransaction> stockTransactions = collectionOfTransactions.find();

        List<StockTransaction> listOfStockTransactions = new ArrayList<>();

        // konvertere fra iterator til list
        for (StockTransaction stockTransaction : stockTransactions) {
			listOfStockTransactions.add(stockTransaction);
		}

        return listOfStockTransactions;
    }

    @Override
    public UserProfile getUserProfile(String username) {
        //MongoCollection<UserProfile> collectionOfUserProfiles = db.getCollection("userprofiles", UserProfile.class);

        FindIterable<UserProfile> user = collectionOfUserProfiles.find(eq("userName", username));
        
        UserProfile first = user.first();

        return first;
    }

    @Override
    public void addUserProfile(UserProfile userProfile) {
        //MongoCollection<UserProfile> collectionOfUserProfiles = db.getCollection("userprofiles", UserProfile.class);

        collectionOfUserProfiles.insertOne(userProfile);
        
    }

}
