package no.bwa.financelog.services;

import java.util.List;

import javax.inject.Inject;

import lombok.extern.slf4j.Slf4j;
import no.bwa.financelog.interfaces.Persistence;
import no.bwa.financelog.models.StockTransaction;

@Slf4j
public class FinancelogService {

    @Inject
    Persistence persistenceService;


    public void addTransaction(StockTransaction stockTransaction) {
        persistenceService.addTransaction(stockTransaction);
            
    }

    public List<StockTransaction> getTransactions(String username) {
        return persistenceService.getTransactions(username);
            
    }
  

    
}
