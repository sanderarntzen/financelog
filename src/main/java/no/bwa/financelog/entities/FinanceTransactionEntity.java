package no.bwa.financelog.entities;

import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRANSACTIONS")

public class FinanceTransactionEntity {
    

    @Id
    @Column(name = "ID", length=36)
    String id;
    String stockName;
    double realizedAmount;
    String currency;
    Date startDate;
    Date endDate;
    
    public FinanceTransactionEntity() {
    }

    public FinanceTransactionEntity(String stockName, double realizedAmount, String currency, Date startDate,
            Date endDate) {
        this.stockName = stockName;
        this.realizedAmount = realizedAmount;
        this.currency = currency;
        this.startDate = startDate;
        this.endDate = endDate;
        this.setId(UUID.randomUUID().toString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public double getRealizedAmount() {
        return realizedAmount;
    }

    public void setRealizedAmount(double realizedAmount) {
        this.realizedAmount = realizedAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    
    
}
