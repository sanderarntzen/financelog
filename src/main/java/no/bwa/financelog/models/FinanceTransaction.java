package no.bwa.financelog.models;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinanceTransaction {

    String id;
    String stockName;
    double realizedAmount;
    Currency currency;
    double realizedAmountNOK;
    LocalDate startDate;
    LocalDate endDate;
}
