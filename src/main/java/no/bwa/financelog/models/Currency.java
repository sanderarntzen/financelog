package no.bwa.financelog.models;

public enum Currency {

    NOK("NOK"), USD("USD"), EUR("EUR");
	
	private String value;
	
	private Currency(String val) {
		this.value = val;
	}
	
	public String getValue() {
		return this.value;
	}

}
