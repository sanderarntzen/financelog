package no.bwa.financelog.models;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockTransaction {
    String id;
    String stockTicker;
    double buyPrice;
    double sellPrice;
    int amountOfShares;
    LocalDateTime buyTime;
    LocalDateTime sellTime;
    double fees;
    String username;
}
