package no.bwa.financelog.resources;


import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@ApplicationScoped
public class MongoProducer {

    @Inject
	@ConfigProperty(name = "mongo.hostname", defaultValue = "localhost")
	String hostname;

	@Inject
	@ConfigProperty(name = "mongo.port", defaultValue = "27017")
	int port;

	@Inject
	@ConfigProperty(name = "mongo.dbname", defaultValue = "financelog")
	String dbName;
	
	
    @Produces
    public MongoClient createMongo() {
    	
    	CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));    	

    	log.info("MongoClient created dbName={}",dbName);
        return new MongoClient(new ServerAddress(hostname,port), new MongoClientOptions.Builder().codecRegistry(pojoCodecRegistry).build());
    }

    @Produces
    public MongoDatabase createDB(MongoClient client) {
        return client.getDatabase(dbName);
    }

    public void close(@Disposes MongoClient toClose) {
        toClose.close();
    }
    
}
