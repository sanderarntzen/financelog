package no.bwa.financelog.resources;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ibm.websphere.security.jwt.Claims;
import com.ibm.websphere.security.social.UserProfileManager;

import no.bwa.financelog.models.UserProfile;
import no.bwa.financelog.services.UserProfileService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/user")
@Singleton
@Transactional

public class UserResource {

    @Inject
    UserProfileService userProfileService;

    private static final Logger log = LoggerFactory.getLogger(UserResource.class);    

    public UserResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response userInfo(
        @Context HttpServletRequest request
        ) {
            String username = request.getUserPrincipal().getName();

            UserProfile userProfile = userProfileService.getUserProfile(username);

            log.info("hello {}", username);
                
            return Response.ok(userProfile).build();
    }

    @GET
    @Path("/logout")
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout(
        @Context HttpServletRequest request
        ) {
            String username = request.getUserPrincipal().getName();
            log.info("hello {}", username);
            String redirectURI = request.getRequestURL().toString().replace("/logout", "/loggedOut");
            URI uri=null;
            try {
                String redirectURIEncoded = URLEncoder.encode(redirectURI, StandardCharsets.UTF_8.toString());
                uri = new URI("https://keycloak.bwa.no/auth/realms/financelog/protocol/openid-connect/logout?redirect_uri=" + redirectURIEncoded);
            } catch (Exception e) {
                log.error("Feil oppstod i logout", e);
            }
        return Response.temporaryRedirect(uri).build();
    }

    @GET
    @Path("/loggedOut")
    @Produces(MediaType.TEXT_HTML)
    public Response loggedOut(
        @Context HttpServletRequest request
        ) {
            String username = request.getUserPrincipal().getName();
            log.info("logging out {}", username);
            try {
                request.logout();
        
                InputStream inputStream = this.getClass().getResourceAsStream("/loggedOut.html");

                String result = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8.toString()))
                    .lines().collect(Collectors.joining("\n"));

                return Response.ok(result).build();        
            } catch (Exception e) {
                log.error("Feil oppstod i loggedOut", e);
            }

        return Response.serverError().build();
    }   
}
