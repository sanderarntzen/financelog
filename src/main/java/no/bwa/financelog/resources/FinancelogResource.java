package no.bwa.financelog.resources;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.bwa.financelog.models.StockTransaction;
import no.bwa.financelog.services.FinancelogService;

@Path("/transaction")
@Singleton
@Transactional

public class FinancelogResource {

    private static final Logger log = LoggerFactory.getLogger(FinancelogResource.class);

    @Inject
    FinancelogService financelogService;
    

    public FinancelogResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<StockTransaction> getTransactionsForUser(@Context HttpServletRequest request) {
        return financelogService.getTransactions(request.getUserPrincipal().getName());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buyStock (@Context HttpServletRequest request, StockTransaction transaction) {
        transaction.setUsername(request.getUserPrincipal().getName());
        financelogService.addTransaction(transaction);
        log.info("Lagret OK: {}", transaction);
        return Response.ok().build();
    }
   
}
