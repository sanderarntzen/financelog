# Financelog

# Lokal utvikling

## Start Liberty Server 
mvn liberty:dev (port: 9080 og 9443)

## React Frontend
Bruk npm v15

cd client
npm install
npm start (localhost:3000) 

# API interface
https://localhost:9443/openapi/ui

# Nginx
C:\WINDOWS\system32>cd d:\nginx-1.21.5

D:\nginx-1.21.5>start nginx.exe

D:\nginx-1.21.5>nginx.exe -s stop

D:\nginx-1.21.5>nginx.exe -s reload

Logge på via nginx
https://financelog-local/api/user

Starter app via nginx
https://financelog-local/ 
C:\WINDOWS\system32\drivers\etc\hosts

# MongoDB

net start mongodb

net stop mongodb


# Diverse
JEE/Jakarta er API-settet for java, for eksempel @GET, @POST 
OpenLiberty kjører java på server

Maven er byggesystem, kopilerer koden, håndterer dependecies

npm (Node Package Management), javascript "sin Maven"